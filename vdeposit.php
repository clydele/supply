<?php
	global $FProductNo;
	
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	
	session_start();
	if(isset($_POST['btn_Print']))
	{
		header('Location:invoicing.php');
	}
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	
	
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}

	
	
		
	
	
?>

<html>
	<head>
		<title>Supply Chain</title>
		<style>
		body{ background-image: url(img/background3.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:100%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;}
				.bodyform table input{width:100%;height:40px;}
				.bodyform table select{width:100%;height:40px;}
				.bodyform table h2{color:green;}
				.bodyform table th{background-color:green;color:white;font:14pt arial;height:40px;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13px arial;}
				.bodyform button{background-color:green;color:white;font:13px arial;width:100%;height:40px;}
				.bodyform input:hover{background-color:white;color:green;}
			.scroll{height:300px;overflow:auto;}
				.scroll table{background-color:white;}
				.scroll table tr td{color:#208000;border-bottom:1px solid green;text-align:center;}
				.scroll table th{background-color:green;color:white;font:14pt arial;height:40px;}
				.scroll table tr td button[name=btn_IUpdate]{background-color:inherit;color:#208000;font:12pt arial;width:70px;border:0px;}
				.scroll table tr td button[name=btn_IClear]{background-color:inherit;color:#208000;font:12pt arial;width:100px;border:0px;}
				.scroll table tr td button[name=btn_ICheckout]{background-color:inherit;color:#208000;font:12pt arial;width:98%;border:0px;margin:auto auto;}
				.scroll table tr td button:hover{background-color:green;color:white;}
			.EditItemDiv{height:210px;width:250px;border:1px solid #208000;position:fixed;margin:auto auto;z-index:1;left:400px;top:200px;background-color:white;display:none;}
				.EditItemDiv table{width:100%;height:100%;margin:auto auto;position:relative;color:green;}
				.EditItemDiv table input,button{width:98%;background-color:green;color:white;} 
		
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
		</style>
		<script>
		
		
		function filldata(clicked)
		{
		
			var a=clicked.split('*');
			var CN=a[0];
			var PD=a[1];
			var LN=a[2];
			var EX=a[3];
			var QU=a[4];
			document.getElementsByName('tb_ProdDesc')[0].value=PD;
			document.getElementsByName('tb_CatNumber')[0].value=CN;
			document.getElementsByName('tb_LotNumber')[0].value=LN;
			document.getElementsByName('dtp_expiration')[0].value=EX;
			document.getElementsByName('tb_Quantity')[0].value=QU;
			return priceList();
		}
		function compute()
		{
			
			var selectedValue=document.getElementsByName('tb_unit')[0].value;
			if(selectedValue.localeCompare("Box/es")==0)
			{
				var quantity=document.getElementsByName('tb_Quantity')[0].value;
				var QPrice=document.getElementsByName('tb_QPrice')[0].value;
				var total=parseFloat(quantity)*parseFloat(QPrice);
				document.getElementsByName('tb_TPrice')[0].value=total.toFixed(2);
			}
			else
			{
				
				var quantity=document.getElementsByName('tb_Quantity')[0].value;
				var QPrice=document.getElementsByName('tb_QPrice')[0].value;
				var total=parseFloat(quantity)*parseFloat(QPrice)/6;
				document.getElementsByName('tb_TPrice')[0].value=total.toFixed(2);
			}			
			
		}
		function prodVal(selected)
		{
			var rawString=selected;
			var parts=selected.split("-");
			document.getElementsByName('tb_ProdDesc')[0].value=parts[1];
			document.getElementsByName('tb_CatNumber')[0].value=parts[0];
			
		}
		function EditItem(clicked)
		{
			document.getElementsByClassName("EditItemDiv")[0].style.display='block';
			var EditedItemDetail=clicked.split('*');
			document.getElementsByName('tb_editedItem')[0].value=EditedItemDetail[1];
			document.getElementsByName('btn_confirmEdit')[0].value=EditedItemDetail[0];
		}
		function ProdAutoFill(curVal)
			{
				var ProdData=curVal.split('*');
				document.getElementsByName('tb_CatNumber')[0].value=ProdData[1];
				document.getElementsByName('tb_ProdDesc')[0].value=ProdData[0];
			    

			}
		

		
	
		</script>
	</head>
	<body>
	
		<form method='POST'>
			<div class='container'>
				<div class='EditItemDiv'>
					<table>
						<tr><td colspan='2'><h3>Edit Item Details</h3></td></tr>
						<tr><td colspan='2'>Name:<input type='text'name='tb_editedItem' disabled/></td>
						<tr><td colspan='2'>Expiration:<input type='date'name='dtp_cExpiration'></td></tr>
						<tr><td colspan='2'>Lot Number:<input type='text'name='tb_cLotNumber'></td></tr>
						<tr>
							<td><button type='submit'name='btn_confirmEdit'>Submit</button></td>
							<td><button type='submit'name='btn_confirmCancel'>Cancel</button></td>
						</tr>
					</table>
				</div>
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>				
							<td><a href='<?php if(strcmp("Rozellyn Yulatic",$_SESSION["COName"])==0){echo "vinventory.php";}else{echo"oinventory.php";}?>'>Inventory</a></td>
							<td><a href='invoice.php'>Invoicing</a></td>							
							<td><a href='sched.php'>Schedules</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Stock In for products</h2></div>
				<div class='bodyform'>
					<table>
							<tr>
							<td colspan='2'>Search:<input type='text'name='tb_Search'></td>	
							<td>&nbsp;<input type='submit'value='SEARCH'name='btn_search'></td>
							<td>&nbsp;<input type='submit'value='Clear'name='btn_Clear'></td>
							</tr>
					</table>
					<table>
						<tr>
							<th style='width:10%;'>Catalog</th>
							<th style='width:30%;'>Product</th>
							<th style='width:15%;'>Lot Number</th>
							<th style='width:15%;'>Expiration</th>
							<th style='width:10%;'>No. of Boxes</th>
							<th style='width:10%;'>Loose Cartridges</th>
							<th style='width:10%;'>Actions</th>
						</tr>
					</table>
				</div>

				<div class='bodyform scroll'>
					
					<table>
											
					</table>
				</div>
				<div class='bodyform'>
					<h2>Added Products</h2>
					<table>
						<tr>
								
						</tr>
					</table>
						
				</div>
				<div class='bodyform scroll'>
					
				</div>
			
			</div>
		
	</body>
</html>
