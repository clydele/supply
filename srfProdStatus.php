<?php
	session_start();
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	

	
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}


	
	
?>
<html>
	<head>
		<title>One AMS Collection</title>
		<style>
			body{ background-image: url(img/background3.jpg);background-size:cover;background-repeat:inherit;}
				.container{width:1000px;height:auto;margin:auto auto;position:relative;}
				.container button:hover{background-color:white;color:green;}
				.container input[type='submit']:hover{background-color:white;color:green;border:1px solid green;}
				.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
				.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
					.navigation table{width:80%;margin:auto auto;position:relative;}
					.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
					.navigation a:hover{background-color:white;color:green;}
					.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
					.navigation input:hover{background-color:white;color:green;}
				.bodyform{width:100%;margin:auto auto;position:relative;padding-top:10px;}
					.bodyform table{width:100%;color:black;border-collapse:collapse;}
					.bodyform table input{width:100%;height:40px;}
					.bodyfrom table tr:last-child {border:1px solid black;}
					.bodyform table tr td{font:13pt arial;color:black;border:1px solid green}
					.bodyform table tr:first-child td{border:0px;}
					.bodyform table h2{color:green;}
					.bodyform table tr th{font:15pt arial;color:white;background-color:green;height:40px;}
					.bodyform input[type=submit]{background-color:green;color:white;font:13pt arial;}
					.bodyform button{background-color:green;color:white;font:13pt arial;width:98%;border:0px;}
					
				.bodyFields{width:90%;margin:auto auto;position:relative;padding-top:10px;}
					.bodyFields table{width:100%;color:black;}
					.bodyFields table input,select{width:100%;height:40px;}
					.bodyFields table h2{color:green;}
					.bodyFields input[type=submit]{background-color:green;color:white;font:13px arial;}
					
				
				.colStatsDiv{width:80%;position:relative;background-color:white;}
					.colStatsDiv table{width:100%;}
					.colStatsDiv table tr td{width:50%;}
					.colStatsDiv table h2{color:white;}
					.colStatsDiv table tr td{border:1px solid green;}
					.colStatsDiv table input{width:100%;height:100%;font:13pt arial;}
				.pageHeader{width:80%;margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
				.InvoiceViewer{height:260px;width:400px;background-color:white;position:fixed;z-index:1;top:250px;left:40%;border:4px solid green;display:none;scroll:auto;text-align:center;}
					.InvoiceViewer button{background-color:green;color:white;font:13pt arial;position:relative;}
					.InvoiceViewer textarea{position:relative;margin;auto auto;width:98%;height:250px;top:6px;}
			
			
		</style>
	</head>
	<body>
		<form method="POST">
			<script>
				function confirmD(clicked)
				{
					var text='Are you sure you want to delete SI#:'
				   document.getElementsByClassName("InvoiceViewer")[0].style.display="block";
				   document.getElementsByClassName("container")[0].style.filter="blur(8px)";
				   document.getElementsByName("ta_invoices")[0].value=text+clicked+'?';
				   document.getElementsByName("btn_CYes")[0].value=clicked;
				   
				
				   
				}
				function hide()
				{
					document.getElementsByClassName("InvoiceViewer")[0].style.display="none";
					document.getElementsByClassName("container")[0].style.filter="blur(-8px)";
				}
				function CustomerAutofill(selected)
				{
					var selectedProd=selected;
					var info=selectedProd.split("*");
					document.getElementsByName("tb_CName")[0].value=info[0];
					document.getElementsByName("tb_CAddress")[0].value=info[1];
					
				}
				function filldata(clicked)
				{
				
					var a=clicked.split('*');
					var CN=a[0];
					var PD=a[1];
					var LN=a[2];
					var EX=a[3];
					var QU=a[4];
					document.getElementsByName('tb_ProdDesc')[0].value=PD;
					document.getElementsByName('tb_CatNumber')[0].value=CN;
					document.getElementsByName('tb_LotNumber')[0].value=LN;
					document.getElementsByName('dtp_expiration')[0].value=EX;
					document.getElementsByName('tb_Quantity')[0].value=QU;
					return priceList();
				}
				function compute()
				{
					
					var selectedValue=document.getElementsByName('tb_unit')[0].value;
					if(selectedValue.localeCompare("Box/es")==0)
					{
						var quantity=document.getElementsByName('tb_Quantity')[0].value;
						var QPrice=document.getElementsByName('tb_QPrice')[0].value;
						var total=parseFloat(quantity)*parseFloat(QPrice);
						document.getElementsByName('tb_TPrice')[0].value=total.toFixed(2);
					}
					else
					{
						
						var quantity=document.getElementsByName('tb_Quantity')[0].value;
						var QPrice=document.getElementsByName('tb_QPrice')[0].value;
						var total=parseFloat(quantity)*parseFloat(QPrice)/6;
						document.getElementsByName('tb_TPrice')[0].value=total.toFixed(2);
					}			
					
				}
				function prodVal(selected)
				{
					var rawString=selected;
					var parts=selected.split("-");
					document.getElementsByName('tb_ProdDesc')[0].value=parts[1];
					document.getElementsByName('tb_CatNumber')[0].value=parts[0];
					
				}
				function EditItem(clicked)
				{
					document.getElementsByClassName("EditItemDiv")[0].style.display='block';
					var EditedItemDetail=clicked.split('*');
					document.getElementsByName('tb_editedItem')[0].value=EditedItemDetail[1];
					document.getElementsByName('btn_confirmEdit')[0].value=EditedItemDetail[0];
				}
				function ProdAutoFill(curVal)
				{
					


					var ProdData=curVal.split('*');
					document.getElementsByName('tb_catalog'+ ProdData[3])[0].value=ProdData[1].toString();
					document.getElementsByName('tb_itemDescription'+ ProdData[3])[0].value=ProdData[0].toString();
					document.getElementsByName('tb_unit'+ ProdData[3])[0].value=ProdData[2].toString();
					
					

						
					
					

				}		
			</script>
			<div class="InvoiceViewer">
				<textarea name='ta_invoices'></textarea>
				<button type='submit'name='btn_CYes'>Yes</button>
				<button type='button'onclick="return hide();">Cancel</button>
				
			</div>
			
			<div class='container'>
				
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
						
							<td><a href='<?php if(strcmp("Rozellyn Yulatic",$_SESSION["COName"])==0){echo "vinventory.php";}else{echo"oinventory.php";}?>'>Inventory</a></td>
							<td><a href='invoice.php'>Invoicing</a></td>							
							<td><a href='sched.php'>Schedules</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Invoice Records</h2></div>
				<div class='bodyFields'>
				<form method='POST'>
					<?php
						if(isset($_SESSION["currentSRF"]))
						{
							$SRFDetailsSQL="Select* from tbl_srf WHERE SRFno=\"".$_SESSION["currentSRF"]."\"";
							$SRFDetails=$con->query($SRFDetailsSQL);
							if($SRFDetails->num_rows>0)
							{
								
								$SRFDetail=$SRFDetails->fetch_assoc()
								

						
					?>
						<table>
							<tr>
								<td colspan='3'>Hospital<input type='text'name='tb_Hospital'value='<?php echo $SRFDetail["Hospital"];?>'></td>
							</tr>
							<tr>
								<td>
									SRF Number
									<input type='text'name='tb_srf_num'value='<?php echo $SRFDetail["SRFno"];?>'>
								</td>
								<td>
									Date Prepared:<input type='date'name='dtp_prepared'value='<?php echo $SRFDetail["date_prepared"];?>'>
								</td>
								<td>
									Dated Needed/Required<input type='date'name='dtp_required'value='<?php echo $SRFDetail["date_required"];?>'>
								</td>
								
							</tr>
						</table>

					<?php
							
							}

						}
						else
						{
							echo"<script>alert('No SRF Selected');</script>";
							header("Location:globalsrf.php");
						}

					?>
					
				</div>
				</form>
				<div class='bodyform'>
					<table>
						<tr>
							<th>Catalog</th>
							<th>Item Description</th>
							<th>Quantity Requested</th>
							<th>Quantity Served</th>
							<th></th>
						</tr>
						<?php
							$SQLSRFProducts="Select* from tbl_srfproducts WHERE SRFNum=\"".$_SESSION["currentSRF"]."\"";
							$SRFProducts=$con->query($SQLSRFProducts);
							if($SRFProducts->num_rows>0)
							{
								while($SRFProduct=$SRFProducts->fetch_assoc())
								{
									echo"
										<tr>
											<td>".$SRFProduct["Catalog"]."</td>
											<td>".$SRFProduct["ProdDescription"]."</td>
											<td>".$SRFProduct["QuantityOrdered"]."</td>
											<td>".$SRFProduct["QuantityServed"]."</td>
										</tr>
									";
								}
							}
						?>
					</table>


				
				</div>
				
			</div>
		</div>
	</form>
	</body>
</html>
