<?php
    $con=new mysqli("localhost","oams","oams@0108","db_salesrep");
    header("Content-Type: application/xls");    
    header("Content-Disposition: attachment; filename=SI Products.xls");  
    header("Pragma: no-cache"); 
    header("Expires: 0");

?>
<table>
<tr>
    <th>SI Number</th>
    <th>Customer</th>
    <th>CatN</th>
    <th>ProdDesc</th>
    <th>Quantity</th>
    <th>Unit</th>
    <th>LotNumber</th>
    <th>Expiration</th>
    <th>Price</th>
    <th>Total</th>
</tr>

<?php
    $VProdsSQL="Select* from tbl_siproducts";
    $VProds=$con->query($VProdsSQL); 
    if($VProds->num_rows>0)
    {
        while($VProd=$VProds->fetch_assoc())
        {
            echo"<tr>
                    <td>".$VProd['SINumber']."</td>
                    <td>".$VProd['Customer']."</td>
                    <td>".$VProd['CatNumber']."</td>
                    <td>".$VProd['ProdDesc']."</td>
                    <td>".$VProd['Quantity']."</td>
                    <td>".$VProd['Unit']."</td>
                    <td>".$VProd['LotNumber']."</td>
                    <td>".$VProd['Expiration']."</td>
                    <td>".$VProd['Price']."</td>
                    <td>".$VProd['Total']."</td>
            
                </tr>";
        }
       

    }
?>