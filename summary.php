<?php
	global $FProductNo;
	
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	session_start();
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	
	
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}
	
	
	
?>

<html>
	<head>
		<title>Reporting System </title>
		<style>
		body{ background-image: url(img/background3.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;}
				.bodyform table input{width:100%;height:40px;}
				.bodyform table h2{color:green;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13px arial;}
				.bodyform button{background-color:green;color:white;font:13px arial;width:100%;height:40px;}
				.bodyform input:hover{background-color:white;color:green;}
			.scroll{height:300px;overflow:auto;}
				.scroll table{background-color:white;}
				.scroll table tr td{color:#208000;border-bottom:1px solid green;}
			
				.scroll table th{background-color:green;color:white;font:14pt arial;height:40px;width:25% !important;}
				.scroll table tr td button[name=btn_IUpdate]{background-color:inherit;color:#208000;font:12pt arial;width:70px;border:0px;}
				.scroll table tr td button[name=btn_IClear]{background-color:inherit;color:#208000;font:12pt arial;width:100px;border:0px;}
				.scroll table tr td button:hover{background-color:green;color:white;}
			
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
		</style>
	</head>
	<body>
		<form method='POST'>
			<div class='container'>
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							
							<td><a href='<?php if(strcmp("Rozellyn Yulatic",$_SESSION["COName"])==0){echo "vinventory.php";}else{echo"oinventory.php";}?>'>Inventory</a></td>
							<td><a href='invoice.php'>Invoicing</a></td>							
							<td><a href='sched.php'>Schedules</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Inventory Monitoring for Other Products</h2></div>
				<div class='bodyform'>
					<table>
							<tr>
							<td colspan='2'>Search:<input type='text'name='tb_Search'></td>	
							<td>&nbsp;<input type='submit'value='SEARCH'name='btn_search'></td>
							</tr>
							<tr>
								<td colspan='2'></td>
								<td><input type='submit'value='Download Product Summary'name='btn_dprodSummary'></td>
								<td><input type='submit'value='Download Invoice Products'name='btn_dSIprod'></td>
							</tr>
					</table>
				</div>
				
				<div class='bodyform scroll'>
					<?php
						if(isset($_POST["btn_dprodSummary"]))
						{
							header("Location:itemSummary.php");
						}
						if(isset($_POST["btn_dSIprod"]))
						{
							header("Location:full.php");
						}
					?>
					<table>
					<tr>
						<th>Catalog</th>
						<th>Item Description</th>
						<th>Total Quantity</th>
						<th>Total Price</th>
					</tr>

					<?php
						$VProdsSQL="Select* from tbl_vprod";
						$VProds=$con->query($VProdsSQL);
						$ProdQSQL="";
						$ProdQs="";
						$ProdQ="";
						
						if($VProds->num_rows>0)
						{
						
							while($VProd=$VProds->fetch_assoc())
							{
								echo"<tr>
								<td>".$VProd['Catalog']."</td>
								<td>".$VProd['Description']."</td>";
								$ProdQSQL="SELECT SUM(Quantity) AS TotalQuantity,SUM(Total) AS TotalPrice from tbl_siproducts WHERE CatNumber='".$VProd['Catalog']."'";
								$ProdQs=$con->query($ProdQSQL);
								if($ProdQs->num_rows>0)
								{
									while($ProdQ=$ProdQs->fetch_assoc())
									{
										echo"
											<td>".$ProdQ['TotalQuantity']."</td>
											<td>".$ProdQ['TotalPrice']."</td>
										</tr>
										";
									}
								}
							}

						}
					?>
			
				
					</table>
					
					
				</div>
				
			</div>
		</div>
	</body>
</html>
